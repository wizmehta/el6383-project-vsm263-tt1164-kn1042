
Project Review
=====================================================


## Overview
1) Briefly summarize the experiment in this project.
The experiment is to observe the change in throughput and resilience time after the interruption of bursty packets in a dumbbell network topology using UDP flow fromm a different client for two competing different TCP variants.
The experimenter has kept three clients, two of them being competing TCP flows and the other being used for introducing interruption via initiating UDP flow. Also, the experimenter has kept three servers to listen to each client at a different time.

2) Does the project generally follow the guidelines and parameters we have
learned in class?
Yes, the project follows the guidelines and parameters we have learned in class. The experimenters have used parameters like TCP variants, throughput and resilience. The parameters affecting performance which we can vary are TCP variants and the parameters not varied is UDP flow.

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of the experiment is to observe the throughput and resilience time of two competing TCP variants by a burst of dominating protocol like UDP. The goal is focused goal, which will tell us, how the throughput will change after initiating UDP flow for different TCP variants and which TCP variant is better in this case. It is likely to have interesting results through which we can see the resilient time of different TCP variants.  The experimenter has also considered the dropping of throughput after UDP flow, which is predetermined but the experimenter has not mentioned about the quantified value of resilient time of TCP variants.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
Yes, the metrics and parameters are chosen appropriately for the experiment. TCP metrics are Throughput and resilient time and the parameters are TCP variant and UDP flow.
The experiment design supports the experiment goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
The experiment is well designed to obtain maximum information with minimum number of trails. Experimenters have taken one trail for each TCP variants, running highspeed in client-1 and varying the TCP variants (Cubic, Reno and Scalable) in client-2 to get the output.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
The metrics selected for the study are right metrics which are Throughput and resilient time. Though, the authors have not mentioned much about the resilient time and is not visualized properly, the metrics are unambiguous and likely to lead to correct conclusions. The goal of the experiment is observe throughput and resilient time, so, throughput and resilient time is suited better for this experiment.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
The parameters (TCP variant and UDP flow) of the experiment are meaningful and help the experiment execute better. There are no variation ranges of parameters i.e. varying parameter ranges does not apply here.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
Yes, the authors have sufficiently addressed the possibility of the interactions between parameters. The experimenters have shown the interaction between the TCP variants and UDP flow. When a UDP flow is introduced for 30 seconds after TCP is ran for 30 seconds, UDP utilizes the bandwidth completely. Then TCP variants again utilizes the bandwidth after the UDP flow is over which can be seen from the graphs presented.






7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
Yes, I feel the comparisons made are reasonable. The comparisons made between different TCP variants implemented at client-1 and client-2 are reasonable. The experimenters have compared highspeed with respect to cubic, reno and scalable under the influence of different dominating protocol of UDP and observed how the throughput is varied before, during and after the influence of UDP.



## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes, the authors report the quantitative results of their experiment but only of the throughput whereas in the goal of the experiment the authors have mentioned about throughput as well as about resilient time. The authors have not shown quantitative results about the resilient time.

2) Is there information given about the variation and/or distribution of
experimental results?
Yes, information is given about the variation of the experimental results. The authors have talked about the variation of throughput under different conditions using different parameters. But the authors have not shown or mentioned about the resilient time in the report.  Though, the resilient time can be seen in the graphs, corresponding to time, but no information is given about the distribution of resilient time.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
Yes, the author have maintained data integrity and avoided ratio games. The results shown by the authors, depict the goal of the experiment correctly. The decrease of throughput and then increase of throughput is shown without using any kind of ratio games.
4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
Yes, the data presented is a clear and effective way. Though it could have been better by comparing TCP variants of client-2 (Cubic, scalable and Reno) in a same graph, which could have shown the competitiveness of the variants among themselves under different protocol like UDP.
5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
The conclusions drawn by the authors sufficiently supports the experiment results. The experimenters have only observed and not made any hard conclusion about the resilient time but only about the throughput makes the experiment results little bit obvious and simple.



## Reproducible research
1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
Authors included the instructions and were pretty easy to follow. The only problem occurred was Rspec was showing error as the Rspec already contained the information about the aggregate.
2) Were you able to successfully produce experiment results?
Yes, the results were successfully reproduced and are pushed into the review/submit folder of the bitbucket.

3) How long did it take you to run this experiment, from start to finish?
By following all the instructions as mentioned, the experiment took around 3-4 hours. Time exceeded as Rspec errors and resources were taking time to get allocated.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
Rspec was needed to be reconstructed as the old Rspec was not working. Took reference of the old topology from the Rspec and made the new one.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
I would characterize it as the results can be reproduced by an independent researcher, requiring considerable effort.

## Other comments to authors
Please write any other comments that you think might help the authors
of this project improve their experiment.

The project has great practical implementation. Except the Rspec error, everything worked well. The description is good for reproducing but more data could have been described to improve the understanding.

Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.

The experiment aims at observing behavior of throughput and resilience time of different TCP variants when interrupted by a flow using UDP
protocol. The experiment set up consist of 3 clients, 3 servers serving respective clients and two routers, one on each side of clients and
servers(dumbbell network topology). Metrics of the project are Throughput of TCP flow and Resilience time of TCP flow. Varying Parameters
used for the experiment is different TCP variants and non-varying parameter is the UDP flow.



2) Does the project generally follow the guidelines and parameters we have
learned in class?

The project does follow the most of the guidelines that we discusses in the class.

1.	The author has stated a specific goal

2.	The metrics has been selected appropriately though the author has forgot to mention UDP flow as one of its parameter along with the TCP
variants.

3.	The project’s quantitative results have been represented using graphs using the Rscript which makes it easy to reproduce the results in
form of graph requiring few manual steps

4.	The generation of shell scripts and execution files provided by the author have made is simpler and faster to reproduce the results but
the report lacks in explaining where to make changes in those files while reproducing the result.





## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal of this project is to observe throughput and resilience time of two competing TCP variants when interrupted by a burst of dominating
protocol like UDP. The goal is specific and focused. The goal of the experiment is likely to have interesting results about the different
resilient time of various TCP variants and about the behavior of throughput of a particular TCP variant when interrupted by another TCP variant
and then by UDP.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

The metrics (throughput and resilient time) and parameters (TCP variants and UDP) chosen are appropriate for this experiment goal. The author
has forgotten to mention UDP as a non-variable parameter in the report. The experiment aims at observing throughput of a two TCP variants
another dominating UDP flow. The experiment design supports the experiment goal.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Yes the experiment has been well designed to obtain maximum information with minimum number of trials.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

Throughput and resilience time have been selected as the metrics for this experiment. The metrics are clear but specific conclusion about
resilient time of various TCP variant cannot be drawn since there is no quantitative result of the resilient time. Graphical observations
related to resillient time are ambiguous.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

TCP variants (varying parameter) and UDP flow (non-varying parameter) have been selected as the parameters for the experiment. The parameters
of the experiment are meaningful since observable and logical changes are seen in the chosen metrics due to change in the parameters. The range
of time intervals for which the TCP and UDP flows are kept on has been selected appropriately for observing throughput and resilient time of
the TCP variants.


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

There is no interaction between the parametrs so the authors has not addressed interaction between parameters.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

The comparison of throughputs of various TCP variants have been drawn for instantaneous results of the experiment. The comparison of resilient
time has been made on the basis of graphs produced using R-script. I found the comparison of resilient time to be ambiguous since it’s the
comparison is not supported by exact numeric values and a precise calculations.



## Communicating results


1) Do the authors report the quantitative results of their experiment?

The author has reported quantitative result of throughput in output.txt files but have not reported quantitative result of resilient time.


2) Is there information given about the variation and/or distribution of
experimental results?

There is no information given on variation of experimental results.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Authors have practiced data integrity. They have not used ratio games and other practices to artificially make their results seem better


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

Throughput has been presented in an effective way. The line graph used to present throughput is appropriate. The author has used same graphs
to represent the resilient time. The author has not computed precise value of resilient time ie does not have any quantitative results of
resilient time. Therefore resilient time has not been presented appropriately .


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

The conclusions drawn about the throughput are supported by the experiment results but since precise numeric value of resilient time has not
been computed the conclusions drawn about resilient is ambiguous.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The author has included most of the things that were required for reproducing the results. Most of the instructions were framed clearly. In
certain places author has not mentioned the changes that were required to be made in order to reproduce the result. Example: in the execution
files and rscripts , the author has not mentioned about changing addresses.


2) Were you able to successfully produce experiment results?

Yes I was able to successfully reproduce the result.


3) How long did it take you to run this experiment, from start to finish?

It took me around 8-10 hrs to understand and interprete the experiment and run it entirely.



4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

No additional steps beyond the documentation was needed in order to successfully complete this experiment.


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

 This experiment can be categorised as a 3rd degree experiment. The results can be reproduced by an independent researcher, requiring
 considerable effort.



## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
The authors should have inserted images in the report for better understanding.
